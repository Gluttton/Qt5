#!/bin/bash

VERSION="5.15.1"

git clone https://github.com/qt/qt5.git
cd qt5
git checkout "v$VERSION"
git submodule update --init -- \
    qtrepotools

perl init-repository \
  --force \
  --module-subset=essential

mkdir -p /opt/qt/$VERSION

export QT_XKB_CONFIG_ROOT=/usr/share/X11/xkb

mkdir -p $(pwd)/build && cd $(pwd)/build

../configure \
   -prefix /opt/qt/$VERSION \
   -release \
   -opensource \
   -confirm-license \
   -c++std c++17 \
   -strip \
   -shared \
   -accessibility \
   -system-freetype \
   -fontconfig \
   -no-qml-debug \
   -nomake examples \
   -nomake tests

make -j$(nproc)
make install

cd ../..
rm -rf qt5

cat config.summary
tree -dL 2 /opt/qt/$VERSION
